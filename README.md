# unzip the folder WORK_Scraper
# navigate to Folders (separately in terminals):  web_scraper and frontend\we-scraper
\n install the node modules with npm install

Files:

Scraper
- run app2.js that scrapes the site to get stuff and save the file in json format

Express server (backend)

- run app.js that acts as the server side. API reads the json file and sends the response to frontend

Angular (frontend)

- run ng serve from terminal (folder: frontend\we-scraper) than goto address localhost:4200 

