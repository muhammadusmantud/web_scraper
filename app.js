const express = require('express')
const fs = require('fs');

const app = express()
const port = 3000


var cors = require('cors')

app.use(cors())

app.get('/', (req, res) => {
    fs.readFile('items_new.json', (err, data) => {
        if (err) throw err;
        let articles = JSON.parse(data);
        console.log(articles);
        res.send(articles)
    });
    // 1 response sends at a time
  //res.send('Hello World!')
 
})

app.listen(port, () => {
  console.log(`app listening on port ${port}`)
})