const axios = require('axios');
const cheerio = require('cheerio');
const fs = require('fs');
const puppeteer = require('puppeteer');

axios.get('https://dev98.de/')
    .then((response) => {
        let $ = cheerio.load(response.data);
        
        $('.site-main article').each((index, element) => {
            //console.log($(element));
            //console.log($(element).find('h2').text().trim());
        });
    })
    .catch((error) => {
        console.log(error);
    });


    function extractItems() {

      /* for (const [i, v] of ['a', 'b', 'c'].entries()) {
        console.log(i, v)
      } */
      console.log('something')
      let dataObj = {};
      let extractedElementstitle = [];
      
      extractedElementstitle = document.querySelectorAll('#main > article');

      const titles =  document.querySelectorAll('#main > article h2');

      const summary  =  document.querySelectorAll('#main > article p');

      /* dataObj['date'] =  document.querySelectorAll('#main > article time.entry-date');
      dataObj['summary'] =  document.querySelectorAll('#main > article entry-summary');
 */
      const items = [];
      for (const [i, v] of titles.entries()) {
        console.log(i, v)
        items.push({
          title: v.innerText,
          article: summary[i].innerText,
          
                  });
      }
      /* for (let element of extractedElementstitle) {
        items.push({
          title: element.title,
          article: element.article,
          
                  });
      } */
      return items;

    }
      async function scrapeInfiniteScrollItems(
        page,
        extractItems,
        itemTargetCount,
        scrollDelay = 1000,
      ) {
        let items = [];
        try {
          let previousHeight;
          while (items.length < itemTargetCount) {
            items = await page.evaluate(extractItems);
            previousHeight = await page.evaluate('document.body.scrollHeight');
            await page.evaluate('window.scrollTo(0, document.body.scrollHeight)');
            await page.waitForFunction(`document.body.scrollHeight > ${previousHeight}`);
            await page.waitForTimeout(scrollDelay);
          }
        } catch(e) { }
        return items;
      }
    
      (async () => {
        // Set up browser and page.
        const browser = await puppeteer.launch({
          headless: false,
          args: ['--no-sandbox', '--disable-setuid-sandbox'],
        });
        const page = await browser.newPage();
        page.setViewport({ width: 1280, height: 926 });

        // Navigate to the demo page.
        /* let i; */ 
        let scrappedObj = [];
        for (var i= 0 ; i<6 ; i++)
        {
        await page.goto(`https://dev98.de/${i ? `page/${i}` :''}`);

        // Scroll and extract items from the page.
        const items = await scrapeInfiniteScrollItems(page, extractItems, 10);
        //console.log('items ... ',items)
        scrappedObj.push(items)
        //console.log('scrappedObj ... ',scrappedObj)
        fs.writeFile(`./items_new.json`, JSON.stringify(scrappedObj), function(err) {
          if(err) console.log('error', err);
        });
        }
        await browser.close();
        
      })();