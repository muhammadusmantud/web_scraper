import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { PostService } from './data.service';
import { Post } from './model/post';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'we-scraper';
  postList: Post[]; 
  //postList: Observable<Post[]>; 
  postListSubs: Subscription;

  constructor(
    private postsapiservice: PostService,
  ) { }
  
  ngOnInit(): void {
    this.postListSubs = this.postsapiservice
    .get_posts()
    .subscribe(postList => {
        this.postList = postList;
        console.log(postList)
      },
      console.error
    );
  }
  ngOnDestroy() {
    this.postListSubs.unsubscribe();
  }
}
