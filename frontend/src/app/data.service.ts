import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse,HttpHeaders} from '@angular/common/http';
import { Observable, EMPTY, throwError} from 'rxjs';
import {catchError } from 'rxjs/operators';
import 'rxjs/add/operator/catch';
import { first } from 'rxjs/operators';
import {API_URL} from './env';
import { Post } from './model/post';

@Injectable()
export class PostService {

  constructor(private http: HttpClient) {
  }

  private static _handleError(err: HttpErrorResponse | any) {
    return Observable.throw(err.message || 'Error: Unable to complete request.');
  }

  // GET list of public, future events
  get_posts(): Observable<Post[]> {
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    headers = headers.append('Access-Control-Allow-Origin', '*');
     let options = { headers: headers };
        return this.http.get<Post[]>(`${API_URL}/`,options)
       .pipe(catchError(PostService._handleError));
  }

}