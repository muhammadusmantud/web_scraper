import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { PostService } from '../data.service';
import { Post } from '../model/post';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.css']
})
export class FeedComponent implements OnInit {
  postList: Post[];
  postListSubs: Subscription;

  constructor(
    private postsapiservice: PostService,
  ) { }

  ngOnInit(): void {
    this.postListSubs = this.postsapiservice
    .get_posts()
    .subscribe(postList => {
        this.postList = postList;
        console.log(postList)
      },
      console.error
    );
  }
  ngOnDestroy() {
    this.postListSubs.unsubscribe();
  }
}

