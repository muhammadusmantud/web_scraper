export class Post {     
    public title: string;
    public summary: string;
    public _id?: number;
  }